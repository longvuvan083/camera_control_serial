#include <stdio.h>
#include <string.h>
#include <iostream>
// Linux headers
#include <fcntl.h>	 // Contains file controls like O_RDWR
#include <errno.h>	 // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h>	 // write(), read(), close()
#include <thread>
#include <vector>
#include <memory>
#include <iomanip>
#include <mutex>
#include <algorithm>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/deadline_timer.hpp>
#include "commands.h"

#define FAIL_CONNECT 1

std::vector<char> testsend, stopcmd, testquery;

int main() {

    std::string serial_in;
    int serial_port = open("/dev/ttyUSB1", O_RDWR);
		if (serial_port < 0) {
			// printf("Error %i from open: %s\n", errno, strerror(errno));
            return FAIL_CONNECT;
		}
	// Create new termios struct, we call it 'tty' for convention
	struct termios tty;

	// Read in existing settings, and handle any error
	if(tcgetattr(serial_port, &tty) != 0) {
		// printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));

		return FAIL_CONNECT;
	}

	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size 
	tty.c_cflag |= CS8; // 8 bits per byte (most common)
	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo
	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	// tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
	// tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

	tty.c_cc[VTIME] = 20;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	// Set in/out baud rate to be 9600
	cfsetispeed(&tty, B9600);
	cfsetospeed(&tty, B9600);

	// Save tty settings, also checking for error
	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
		// printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
		return -1;
	}

    char read_buf [7];
    memset(&read_buf, '\0', sizeof(read_buf));
    // testsend.push_back(0xff);
    // testsend.push_back(0x01);
    // testsend.push_back(0x00);
    // testsend.push_back(0x04);
    // testsend.push_back(0x03);
    // testsend.push_back(0x06);
    // testsend.push_back(0x0e);
    
    testquery.push_back(0xff);
    testquery.push_back(0x01);
    testquery.push_back(0x00);
    testquery.push_back(0x51);
    testquery.push_back(0x00);
    testquery.push_back(0x00);
    testquery.push_back(0x52);

    stopcmd.push_back(0xff);
    stopcmd.push_back(0x01);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x01);

    // std::cout << "Msg to be sent: ";
    // for (char c : testsend)
    //   printf("%x", (uint8_t) c);
    //   // printf("\\x%x", (uint8_t) c);
    // std::cout << "\n";
    for (int i = 0; i < 100; i++) {
        write(serial_port, (void *)testquery.data(), sizeof(testquery));
        int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));

        write(serial_port, (void *)stopcmd.data(), sizeof(stopcmd));
        if (num_bytes < 0) {
            printf("Error reading: %s", strerror(errno));
        }
        else
        {
            std::cout << "Msg received: ";
            for (char c : read_buf)
            {
                printf("\\x%02x ", (uint8_t) c);
                // response.push_back((boost::uint8_t)c);
            }
            std::cout << "\n";
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(30));
    }
    return 0;
}