#pragma once
// C library headers
#include <stdio.h>
#include <string.h>
#include <iostream>
#include "serial_connection.h"
std::unique_ptr<Camera_serial> cam_serial;
std::vector<char> testsend, stopcmd;

CamPosition_t camPosition;

int main()
{
    cam_serial = std::unique_ptr<Camera_serial>(new Camera_serial("/dev/ttyUSB1", 9600));
    testsend.push_back(0xff);
    testsend.push_back(0x01);
    testsend.push_back(0x00);
    testsend.push_back(0x04);
    testsend.push_back(0x03);
    testsend.push_back(0x06);
    testsend.push_back(0x0e);
    // printf("%x %x %x %x %x %x %x\n", testsend[0], testsend[1], testsend[2], testsend[3], testsend[4], testsend[5], testsend[6]);
     //ff010051000052  ff010053000054
    std::vector<char> msg = std::vector<char>(7);
    
    // cout << "Msg to be sent: ";
    // for (char c : testsend)
    //   printf("%x", (uint8_t) c);
    //   // printf("\\x%x", (uint8_t) c);
    // cout << "\n";

    stopcmd.push_back(0xff);
    stopcmd.push_back(0x01);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x00);
    stopcmd.push_back(0x01);

    // cout << "Msg to be sent: ";
    // for (char c : stopcmd)
    //   printf("%x", (uint8_t) c);
    //   // printf("\\x%x", (uint8_t) c);
    // cout << "\n";
    cam_serial->init_connection();
    std::cout << " done init \n";
    // cam_serial->write_serial_nores(testsend);
    // std::cout << " done test write_serial_nores \n";

    // std::this_thread::sleep_for(std::chrono::seconds(1));
    // std::this_thread::sleep_for(std::chrono::milliseconds(30));
    // std::cout << "[LOG] 1 \n";
    std::vector<boost::uint8_t> data_v;
    // std::vector<char> msg = std::vector<char>(7);
    for (int idx = 0; idx < 100; idx++) {
        // cam_serial->queryPan(msg, data_v, 1);
        std::string output3 = cam_serial->queryPosition(camPosition, 1, 1.0);
        std::cout << "output = " << output3 << "\n";
        
    }

    // std::cout << "[LOG] 2 \n";
    // std::string output1 = cam_serial->queryTilt(msg, 1);
    // std::cout << "[LOG] 3 \n";
    // std::string output2 = cam_serial->queryZoom(msg, 1);

    // std::string output3 = cam_serial->queryPosition(camPosition, 1, 1.0);
    // std::cout << "output = " << output3 << "\n";

    // cam_serial->write_serial_nores(stopcmd);
    // std::this_thread::sleep_for(std::chrono::milliseconds(30));
return 0; // success
};