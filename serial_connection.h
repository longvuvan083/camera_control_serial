#pragma once

#include <iostream>
#include <string>
// Linux headers
#include <fcntl.h>	 // Contains file controls like O_RDWR
#include <errno.h>	 // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h>	 // write(), read(), close()
#include <thread>
#include <vector>
#include <memory>
#include <iomanip>
#include <mutex>
#include <algorithm>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/high_resolution_timer.hpp>
#include <boost/asio/deadline_timer.hpp>
#include "commands.h"

using namespace std;
// using namespace boost::asio;
using std::cout;
using std::endl;
using std::string;

unsigned long get_baud(unsigned long baud) ; 

enum class ConnectionState
{
	Connected = 0,
	Waiting = 1,
	Refused = 2,
	Closed = 3,
};

enum class ParsingState
{
	WaitingHeader = 0,
	WaitingAdressCam = 1,
	WaitingInfoCam = 2,
	WaitingResponseCode1 = 3,
	WaitingResponseCode2 = 4,
	WaitingData1 = 5,
	WaitingData2 = 6,
	WaitingChecksum = 7,
	EndOfParsing = 8,
};

enum class DataPanTiltZoom
{
	Unknown = 0,
	Pan = 1,
	Tilt = 2,
	Zoom = 3,
};

struct CamPosition_t
{
	CamPosition_t() : tilt{ 0 }, tilt_nan{ true }, pan{ 0 }, pan_nan{ true }, fov{ 0 }, fov_nan{ true } {}
	float tilt;
	bool tilt_nan = true;
	float pan;
	bool pan_nan = true;
	float fov;
	bool fov_nan = true;
	friend std::ostream& operator<<(std::ostream& os, const CamPosition_t& pos)
	{
		return os << "Pan: " << (pos.pan_nan ? "nan" : (std::to_string(pos.pan) + " deg"))
			<< " | Tilt: " << (pos.tilt_nan ? "nan" : (std::to_string(pos.tilt) + " deg"))
			<< " | FOV: " << (pos.fov_nan ? "nan" : (std::to_string(pos.fov) + " deg"));
	}
	std::string str(float north_bearing_offset)
	{
		std::ostringstream oss;

		// Print Pan
		float pan2stream = pan - north_bearing_offset;
		while (pan2stream < 0)
			pan2stream += 360.0f;
		while (pan2stream > 360.0f)
			pan2stream -= 360.0f;
		oss << "Pan: ";
		if (pan_nan)
			oss << "nan";
		else
			oss << std::fixed << std::setprecision(2) << pan2stream << " deg";
		// Print Tilt
		oss << "| Tilt: ";
		if (tilt_nan)
			oss << "nan";
		else
			oss << std::fixed << std::setprecision(2) << tilt << " deg";
		// Print Fov
		oss << " | FOV: ";
		if (fov_nan)
			oss << "nan";
		else
			oss << std::fixed << std::setprecision(2) << fov << " deg";

		return oss.str();
	}
};


#define FAIL_CONNECT 1

class Camera_serial
{
private:
	std::string serial_in;
	int baud_rate;
	int serial_port;

	std::string m_version = "0.2.11";
	bool m_verbose_debug = false;
	bool m_connected_status = false;
	// Connection details
	int m_connection_priority;
	// Connection context
	// Internal setup
	std::vector<std::vector<float>> m_lut;
	mutable std::mutex m_mutex;
	int current_camera;
	// new parse response
	uint16_t checksum;
	uint8_t data1;
	uint8_t data2;
	uint16_t data;
	DataPanTiltZoom ptz;
	ParsingState ps;

public:
	Camera_serial(std::string serial_in, int baud_rate) : serial_in{serial_in}, baud_rate{baud_rate} {}
	~Camera_serial();
	int init_connection();

	bool check_connection_status();
	// std::string write_serial_nores(std::vector<char> &msg);
	void  write_serial_nores(std::vector<char> &msg);
	void write_serial(std::vector<char> &msg, std::vector<boost::uint8_t>& response);

	// Other stuff
	float current_fov = 0;
	float max_fov;
	float min_fov;

	// Camera manager messages
	void testConnection(std::vector<char> &msg);
	void setPriority(std::vector<char> &msg, int priority = 100, int cam_address = 1);

	// Query messages
	std::string queryPosition(CamPosition_t &camPosition, int cam_address = 1, float north_bearing_offset = 0.0f);
	void queryPan(std::vector<char> &msg, std::vector<boost::uint8_t> &response, int cam_address = 1);
	void queryTilt(std::vector<char> &msg, std::vector<boost::uint8_t> &response, int cam_address = 1);
	void queryZoom(std::vector<char> &msg, std::vector<boost::uint8_t> &response, int cam_address = 1);

	// Getters for basic information
	int getZoom(std::vector<char> &msg, int cam_address = 1);

	// PTZ command messagess relative
	void panLeft(std::vector<char> &msg, int pan_speed = 1, int cam_address = 1);
	void tiltPanUpLeft(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void tiltUp(std::vector<char> &msg, int tilt_speed = 1, int cam_address = 1);
	void tiltPanUpRight(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void panRight(std::vector<char> &msg, int pan_speed = 1, int cam_address = 1);
	void tiltPanDownLeft(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void tiltDown(std::vector<char> &msg, int tilt_speed = 1, int cam_address = 1);
	void tiltPanDownRight(std::vector<char> &msg, int pan_speed = 1, int tilt_speed = 1, int cam_address = 1);
	void zoomIn(std::vector<char> &msg, int cam_address = 1);
	void zoomOut(std::vector<char> &msg, int cam_address = 1);

	void stopCamera(std::vector<char> &msg, int cam_address = 1);

	void parse_response(std::vector<boost::uint8_t> response);

	// PTZ command messages absolute in degrees and raw form (zoom is equivalent)
	void panAbsoluteDeg(std::vector<char>& msg, int pan = 9000, int cam_address = 1);
	void panAbsoluteRaw(std::vector<char>& msg, int pan = 0, int cam_address = 1);

	void tiltAbsoluteDeg(std::vector<char>& msg, int tilt = 0, int cam_address = 1);
	void tiltAbsoluteRaw(std::vector<char>& msg, int tilt = 0, int cam_address = 1);

	void panAbsolute(std::vector<char> &msg, int pan = 9000, int cam_address = 1);
	void tiltAbsolute(std::vector<char> &msg, int tilt = 0, int cam_address = 1);
	void zoomAbsolute(std::vector<char> &msg, int zoom = 1, int cam_address = 1);

	// Extra FoV commands
	void fovAbsolute(std::vector<char> &msg, float fov = 1, int cam_address = 1);
	void changeZoomByFoV(std::vector<char> &msg, float fov_change = 1, int cam_address = 1);

	// Zoom to FoV conversion
	std::vector<std::vector<float>> loadLut(std::string filename);
	float zoomToFov(int zoom, std::vector<std::vector<float>> lut);
	float fovToZoom(float fov, std::vector<std::vector<float>> lut);
	// Centering
	void moveTowardsCenter(int x_target, int y_target, int x_current, int y_current, std::string& camera_mode, int& x_speed, int& y_speed, int tolerance_radius = 10, float max_size_vector = 100, int max_speed = 20, int bouder_for_extra = 0, float random_number_for_extra = 0, int magic_point = 0, int camera_address = 1, bool loging_camera = false);
	std::vector<float> moveAbs(int x_target, int y_target, int x_current, int y_current, CamPosition_t camPosition, int camera_address = 1);
	void test();

};

