#pragma once
#include "serial_connection.h"
#include <iostream>
#include <string>
// Linux headers
#include <fcntl.h>	 // Contains file controls like O_RDWR
#include <errno.h>	 // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h>	 // write(), read(), close()
#include <thread>
#include <string.h>
#include <sstream>
// 
#include <boost/lambda/lambda.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <optional>
#include <iomanip>
#include <iterator>
#include <algorithm>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <bitset>
#include <boost/system/error_code.hpp>
#include <boost/optional.hpp>
// #include <boost/system.hpp>
#include <boost/bind.hpp>

// using namespace boost::asio;
using std::cout;
using std::endl;
using std::string;

unsigned long get_baud(unsigned long baud)
{
    switch (baud) {
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    default: 
        return B0;
    }
}
int Camera_serial::init_connection() {
// Open the serial port. Change device path as needed (currently set to an standard FTDI USB-UART cable type device)
	serial_port = open(serial_in.c_str(), O_RDWR);
		if (serial_port < 0) {
			// printf("Error %i from open: %s\n", errno, strerror(errno));
            return FAIL_CONNECT;
		}
	// Create new termios struct, we call it 'tty' for convention
	struct termios tty;

	// Read in existing settings, and handle any error
	if(tcgetattr(serial_port, &tty) != 0) {
		// printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));

		return FAIL_CONNECT;
	}

	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size 
	tty.c_cflag |= CS8; // 8 bits per byte (most common)
	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo
	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	// tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
	// tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

	tty.c_cc[VTIME] = 20;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	// Set in/out baud rate to be 9600
	cfsetispeed(&tty, get_baud(baud_rate));
	cfsetospeed(&tty, get_baud(baud_rate));

	// Save tty settings, also checking for error
	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
		// printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
		return FAIL_CONNECT;
	}
}

void Camera_serial::write_serial(std::vector<char> &msg, std::vector<boost::uint8_t> &response) {
    // std::lock_guard<std::mutex> lock(m_mutex);
	m_verbose_debug = 0;
    if (m_verbose_debug)
    {
        cout << "Msg to be sent: "; // << string(msg.begin(), msg.end()) << std::endl;
		for (char c : msg)
		  printf("%x", (uint8_t) c);
		cout << "\n";
    }
    write(serial_port, (void *)msg.data(), sizeof(msg));
    char read_buf [7];
    memset(&read_buf, '\0', sizeof(read_buf));
    int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));
    // n is the number of bytes read. n may be 0 if no bytes were received, and can also be -1 to signal an error.
    std::this_thread::sleep_for(std::chrono::milliseconds(25));
    if (num_bytes < 0) {
        printf("Error reading: %s", strerror(errno));
    }
    else
	{
		for (char c : read_buf)
		{
			printf("\\x%02x ", (uint8_t) c);
			response.push_back((boost::uint8_t)c);
		}
		std::cout << "\n";
	}
}

void Camera_serial::write_serial_nores(std::vector<char> &msg) {
	m_verbose_debug = 1;
    if (m_verbose_debug)
    {
		for (char c : msg)
			printf("%x", (uint8_t) c);
		std::cout << "\n";
    }
    write(serial_port, (void *)msg.data(), sizeof(msg));
	std::this_thread::sleep_for(std::chrono::milliseconds(25));
}

void Camera_serial::queryPan(std::vector<char>& msg, std::vector<boost::uint8_t> &response, int cam_address)
{
    cout << "Querying PAN" << std::endl;
    msg[0] = 0xff;
    msg[1] = cam_address % 256;
    msg[2] = 0x00;
    msg[3] = 0x51;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
    // printf("%x %x %x %x %x %x %x\n", msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6]);
    write_serial(msg, response);
}

void Camera_serial::queryTilt(std::vector<char>& msg, std::vector<boost::uint8_t> &response, int cam_address)
{
    cout << "Querying TILT" << std::endl;
    msg[0] = 0xff;
    msg[1] = cam_address % 256;
    msg[2] = 0x00;
    msg[3] = 0x53;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
    write_serial(msg, response);
}

void Camera_serial::queryZoom(std::vector<char>& msg, std::vector<boost::uint8_t> &response, int cam_address)
{
    cout << "Querying ZOOM" << std::endl;
    msg[0] = 0xff;
    msg[1] = cam_address % 256;
    msg[2] = 0x00;
    msg[3] = 0x55;
    msg[4] = 0x00;
    msg[5] = 0x00;
    msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
    write_serial(msg, response);
}

int Camera_serial::getZoom(std::vector<char>& msg, int cam_address)
{
    std::vector<boost::uint8_t> data_v;
    std::uint8_t data0;
    std::uint8_t data1;
    // display the current camera zoom / FoV level.
    queryZoom(msg, data_v, cam_address);
    // if data is received
    if (data_v.size() > 0)
    {
        // data = data.substr(4, 2); // filters data bytes received
        //                         // cast bytes to unsigned int
        data0 = data_v[4];
        data1 = data_v[5];

        int zoom_raw = int((data0 << 8) | data1); // combines bytes via bitwise shift

        return zoom_raw;
    }
    else
    {
        return 0;
    }
}

std::string Camera_serial::queryPosition(CamPosition_t &camPosition, int cam_address, float north_bearing_offset)
{
	std::vector<char> msg = std::vector<char>(7);
	std::vector<boost::uint8_t> response_tcp;
	std::string response_string = "";
	std::stringstream stream;

	try
	{
		std::vector<boost::uint8_t> data_v;
		queryPan(msg, data_v, cam_address);
		queryTilt(msg, data_v, cam_address);
		queryZoom(msg, data_v, cam_address);
		std::cout << "\t[ LOG ] data_v.size: " << data_v.size() << std::endl;

		for (boost::uint8_t res : data_v)
		{
			switch (ps)
			{
			case ParsingState::WaitingHeader:
				cout << "\t[ LOG ]ParsingState::WaitingHeader\n";
				if (res == RESPONSE_HEADER)
				{
					data = 0;
					data1 = 0;
					data2 = 0;
					checksum = 0;
					ps = ParsingState::WaitingAdressCam;
					ptz = DataPanTiltZoom::Unknown;
				}
				break;

			case ParsingState::WaitingAdressCam:
				cout << "\t[ LOG ]ParsingState::WaitingAdressCam\n";
				checksum += res;
				ps = ParsingState::WaitingResponseCode1;
				break;

			case ParsingState::WaitingResponseCode1:
				cout << "\t[ LOG ]ParsingState::WaitingResponseCode1\n";
				if (res == RESPONSE_OK_MSB || res == RESPONSE_QUERY_PAN_MSB || res == RESPONSE_QUERY_TILT_MSB || res == RESPONSE_QUERY_ZOOM_MSB)
					ps = ParsingState::WaitingResponseCode2;
				else
				{
					ps = ParsingState::WaitingHeader;
				}
				checksum += res;

				break;

			case ParsingState::WaitingResponseCode2:
				cout << "\t[ LOG ]ParsingState::WaitingResponseCode2\n";
				if (res == RESPONSE_OK_LSB)
				{
					ps = ParsingState::WaitingHeader;
				}
				else if (res == RESPONSE_QUERY_PAN_LSB)
				{
					ps = ParsingState::WaitingData1;
					ptz = DataPanTiltZoom::Pan;
				}
				else if (res == RESPONSE_QUERY_TILT_LSB)
				{
					ps = ParsingState::WaitingData1;
					ptz = DataPanTiltZoom::Tilt;
				}
				else if (res == RESPONSE_QUERY_ZOOM_LSB)
				{
					ps = ParsingState::WaitingData1;
					ptz = DataPanTiltZoom::Zoom;
				}
				else
				{
					ps = ParsingState::WaitingHeader;
				}
				checksum += res;
				break;

			case ParsingState::WaitingData1:
				cout << "\t[ LOG ]ParsingState::WaitingData1\n";
				data1 = res;
				ps = ParsingState::WaitingData2;
				checksum += res;
				break;

			case ParsingState::WaitingData2:
				cout << "\t[ LOG ]ParsingState::WaitingData2\n";
				data2 = res;
				ps = ParsingState::WaitingChecksum;
				checksum += res;
				break;

			case ParsingState::WaitingChecksum:
				cout << "\t[ LOG ]ParsingState::WaitingChecksum\n";
				if (res == (checksum % 256))
				{
					uint16_t data = ((data1 << 8) | data2);

					if (ptz == DataPanTiltZoom::Pan)
					{
						cout << "\t[ LOG ] 1\n";
						camPosition.pan = static_cast<float>(data) / 100.0f;
						while (camPosition.pan > 180.0)
							camPosition.pan -= 360.0f;
						camPosition.pan_nan = false;
					}
					else if (ptz == DataPanTiltZoom::Tilt)
					{
						cout << "\t[ LOG ] 2\n";
						float tilt_raw = static_cast<float>(data) / 100.0f;
						camPosition.tilt = (tilt_raw < 180 ? tilt_raw * -1 : 360 - tilt_raw); // converts to tilt level
						camPosition.tilt_nan = false;
					}

					else if (ptz == DataPanTiltZoom::Zoom)
					{
						cout << "\t[ LOG ] 3\n";
						std::cout << " [LOG] dm chet o day \n";
						camPosition.fov = zoomToFov(data, m_lut);
						camPosition.fov_nan = false;
					}

					ps = ParsingState::WaitingHeader;
					cout << "\t[ LOG ] 4\n";
				}
				else
				{
					ps = ParsingState::WaitingHeader;
				}
				break;

			default:
				cout << "\t[ LOG ]default\n";
				ps = ParsingState::WaitingHeader;
				break;
			}
		}
		// std::vector<uint8_t> data_v{reinterpret_cast<uint8_t>(data.c_str())};
		// response_string += std::to_string(data_v.size());
		// camPosition.pan = 0;
		// camPosition.tilt = 0;
		// camPosition.fov = 0;
	}
	catch (...)
	{
		std::cout << " [LOG] go to catch \n";
		if (m_verbose_debug)
		{
			cout << "Boost sys error - could not complete query" << endl;
		}
		m_connected_status = false;
		return "";
	}
	std::cout << " [log] before camPosition.str(north_bearing_offset)" << "\n";
	std::cout << " [log] camPosition.str(north_bearing_offset)" <<  camPosition.str(north_bearing_offset) << "\n";
	return camPosition.str(north_bearing_offset);
}

void Camera_serial::panLeft(std::vector<char>& msg, int pan_speed, int cam_address)
{
	// cout << "Moving LEFT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x04;
	msg[4] = pan_speed % 256;
	msg[5] = 0x00;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::tiltPanUpLeft(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	// cout << "Moving UPLEFT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x0c;
	msg[4] = pan_speed % 256;
	msg[5] = tilt_speed % 256;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::tiltUp(std::vector<char>& msg, int tilt_speed, int cam_address)
{
	// cout << "Moving RIGHT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x08;
	msg[4] = 0x00;
	msg[5] = tilt_speed % 256;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::tiltPanUpRight(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	// cout << "Moving UPRIGHT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x0a;
	msg[4] = pan_speed % 256;
	msg[5] = tilt_speed % 256;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::panRight(std::vector<char>& msg, int pan_speed, int cam_address)
{
	// cout << "Moving RIGHT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x02;
	msg[4] = pan_speed % 256;
	msg[5] = 0x00;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::tiltPanDownLeft(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	// cout << "Moving DOWNLEFT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x14;
	msg[4] = pan_speed % 256;
	msg[5] = tilt_speed % 256;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::tiltDown(std::vector<char>& msg, int tilt_speed, int cam_address)
{
	// cout << "Moving DOWN" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x10;
	msg[4] = 0x00;
	msg[5] = tilt_speed % 256;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::tiltPanDownRight(std::vector<char>& msg, int pan_speed, int tilt_speed, int cam_address)
{
	// cout << "Moving DOWNRIGHT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x12;
	msg[4] = pan_speed % 256;
	msg[5] = tilt_speed % 256;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::zoomIn(std::vector<char>& msg, int cam_address)
{
	// cout << "Moving ZOOMIN" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x20;
	msg[4] = 0x00;
	msg[5] = 0x00;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::zoomOut(std::vector<char>& msg, int cam_address)
{
	// cout << "Moving ZOOMOUT" << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x40;
	msg[4] = 0x00;
	msg[5] = 0x00;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::stopCamera(std::vector<char>& msg, int cam_address)
{
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x00;
	msg[4] = 0x00;
	msg[5] = 0x00;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

// Note: input is in hundredths of a degree between 0 (pointing directly behind default) and 17999 (90deg pointing up)
void Camera_serial::panAbsolute(std::vector<char>& msg, int pan, int cam_address)
{
	pan %= 36000;
	char lsb = pan % 256;
	char msb = (pan >> 8) % 256;

	// cout << "Moving PAN absolute to: " << pan << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x4B;
	msg[4] = msb;
	msg[5] = lsb;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

// Note: input is in hundredths of a degree between -9000 (90deg pointing down) and 9000 (90deg pointing up)
void Camera_serial::tiltAbsolute(std::vector<char>& msg, int tilt, int cam_address)
{
	tilt %= 36000;
	// Converting tilt input to hex using bitshift
	char lsb = tilt % 256;
	char msb = (tilt >> 8) % 256;

	// cout << "Moving TILT absolute to: " << tilt << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x4D;
	msg[4] = msb;
	msg[5] = lsb;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}


void Camera_serial::zoomAbsolute(std::vector<char>& msg, int zoom, int cam_address)
{
	// Converting tilt input to hex using bitshift
	char lsb = zoom % 256;
	char msb = (zoom >> 8) % 256;

	// cout << "Moving ZOOM absolute to: " << zoom << endl;
	msg[0] = 0xff;
	msg[1] = cam_address % 256;
	msg[2] = 0x00;
	msg[3] = 0x4F;
	msg[4] = msb;
	msg[5] = lsb;
	msg[6] = (msg[1] + msg[2] + msg[3] + msg[4] + msg[5]) % 256;
	// std::cout << "[ LOG ] zoomAbsolute sent: ";
	// for (int i = 0; i < 7; ++i)
	// {
	// 	std::cout << "\\x" << std::setfill('0') << std::hex << std::setw(2) << static_cast<unsigned int>(msg[i]);
	// }
	// std::cout << std::endl;
	// write_serial(msg, 4);
	write_serial_nores(msg);
}

void Camera_serial::fovAbsolute(std::vector<char>& msg, float fov, int cam_address)
{
	// Calculate zoom level
	int zoom = fovToZoom(fov, m_lut);
	// Use absolute zoom
	zoomAbsolute(msg, zoom, cam_address);
}

void Camera_serial::changeZoomByFoV(std::vector<char>& msg, float fov_change, int cam_address)
{

	// Calculate new FoV based on the current zoom and Fov
	float new_fov = zoomToFov(getZoom(msg, cam_address), m_lut) + fov_change;

	// Use absolute FoV command to move towards new FoV
	fovAbsolute(msg, new_fov, cam_address);
}

std::vector<float> Camera_serial::moveAbs(int x_target, int y_target, int x_current, int y_current, CamPosition_t camPosition, int camera_address) {
	std::vector<char> msg = std::vector<char>(7);
	std::vector<float> newPanTilt;

	//std:string camera_status_tmp = cam_client_->queryPosition(camPosition);
				// std::cout << "EVENT_LBUTTONDOUBLEBLCLK" << std::endl;
	float deltaHorizontal = atan(abs(x_target - static_cast<int>(x_current)) * tan(camPosition.fov * M_PI / 360.0) / x_current);
	//std::cout << "[ camPosition.pan ]: " << -1 * camPosition.pan << std::endl;
	float newPan = camPosition.pan + deltaHorizontal * 180.0 * M_1_PI * ((x_target - x_current) < 0 ? -1 : 1);
	//std::cout << "[ new pan ]: " << newPan << std::endl;
	float fov_vertical_2 = atan(y_current * tan(camPosition.fov * M_PI / 360.0) / x_current);
	//std::cout << "[ fov_vertical_2 ]: " << fov_vertical_2 * M_1_PI * 180 << std::endl;
	float deltaVertical = atan(abs(y_target - static_cast<int>(y_current)) * tan(fov_vertical_2) / y_current);
	//std::cout << "[ deltaVertical ]:  " << deltaVertical * M_1_PI * 180 << std::endl;
	float newTilt = -1 * camPosition.tilt + deltaVertical * M_1_PI * 180.0 * ((y_target - y_current) < 0 ? -1 : 1) + 360.0;
	//std::cout << "[ camPosition.tilt ]: " << -1 * camPosition.tilt << std::endl;
	//std::cout << "[ newTilt ]: " << newTilt<<"\t" << static_cast<int>(newTilt * 100) << std::endl;

	newPanTilt.push_back(newPan);
	newPanTilt.push_back(newTilt);

	//if (manager_status == "ONLINE") {
	//newPan += config.director.north_bearing_offset;
	while (newPan < 0)
		newPan += 360.0f;
	while (newPan > 360.0f)
		newPan -= 360.0f;

	panAbsolute(msg, static_cast<int>(newPan * 100), camera_address);
	tiltAbsolute(msg, static_cast<int>(newTilt * 100), camera_address);

	while (newTilt < -180.0f)
		newTilt += 360.0f;
	while (newTilt > 180.0f)
		newTilt -= 360.0f;

	newPanTilt.push_back(newPan);
	newPanTilt.push_back(-1 * newTilt);

	return newPanTilt;
}

void Camera_serial::test() {
	std::vector<char> msg = std::vector<char>(7);
	panRight(msg, 30, 1);
}

void Camera_serial::moveTowardsCenter(int x_target, int y_target, int x_current, int y_current, std::string& camera_mode, int& x_speed_, int& y_speed_, int tolerance_radius, float max_size_vector, int max_speed, int bouder_for_extra, float random_number_for_extra, int magic_point, int camera_address, bool loging_camera)
{
	// std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
	// auto duration = now.time_since_epoch();
	// auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

	// std::cout << "1: " << millis << "\n";
	std::vector<char> msg = std::vector<char>(7);

	// Get x, y offsets:
	int x_offset = x_target - x_current; // positive = move right; negative = move left
	int y_offset = y_target - y_current; // positive = move down; negative = move up
	int extra_speed_x = 0;
	int extra_speed_y = 0;

	// std::cout << "bouder_for_extra: " << bouder_for_extra << " x_offset: " << x_offset << " bouder_for_extra: " << bouder_for_extra << " magic_point: " << magic_point << "\n";

	if (bouder_for_extra != 0 && std::abs(x_offset) <= bouder_for_extra && std::abs(x_offset) > magic_point)
	{
		extra_speed_x = random_number_for_extra / std::abs(x_offset);
		// std::cout << "extra_speed_x---\n";
	}

	if (bouder_for_extra != 0 && std::abs(y_offset) <= bouder_for_extra && std::abs(y_offset) > magic_point)
	{
		extra_speed_y = random_number_for_extra / std::abs(y_offset);
		// std::cout << "extra_speed_y---\n";
	}

	// std::cout << "extra x: " << extra_speed_x << " extra y: " << extra_speed_y << "\n";

	// Speed to be used
	int speed;

	if (m_verbose_debug)
	{
		cout << "x offset: " << x_offset << " | y offset: " << y_offset << endl;
	}

	// check if offset is within tolerance
	if (abs(x_offset) <= tolerance_radius && abs(y_offset) <= tolerance_radius)
	{
		// return stop command
		stopCamera(msg, camera_address);
		camera_mode = "Standby";
	} // if x offset is zero, move vertically
	else if (x_offset == 0)
	{
		if (y_offset > 0)
		{
			// get speed
			speed = y_offset / max_size_vector;
			speed = speed + extra_speed_y;
			speed = speed < max_speed ? speed : max_speed;
			// move down
			y_speed_ = -1 * speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "tilt : " << millis << "\n";*/
			tiltDown(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done tilt: " << millis << "\n";*/
		}
		else
		{
			// get speed
			speed = abs(y_offset) / max_size_vector;
			speed = speed + extra_speed_y;
			speed = speed < max_speed ? speed : max_speed;
			// move up
			y_speed_ = speed;
			/*	now = std::chrono::steady_clock::now();
				duration = now.time_since_epoch();
				millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
				std::cout << "tilt : " << millis << "\n";*/
			tiltUp(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done tilt: " << millis << "\n";*/
		}
	} // if y offset is zero, move horizontally
	else if (y_offset == 0)
	{
		if (x_offset > 0)
		{
			// get speed
			speed = x_offset / max_size_vector;
			speed = speed + extra_speed_x;
			speed = speed < max_speed ? speed : max_speed;
			// move right
			x_speed_ = speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan: " << millis << "\n";*/
			panRight(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan: " << millis << "\n";*/
		}
		else
		{
			// get speed
			speed = abs(x_offset) / max_size_vector;
			speed = speed + extra_speed_x;
			speed = speed < max_speed ? speed : max_speed;
			// move left
			x_speed_ = -1 * speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan: " << millis << "\n";*/
			panLeft(msg, speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan: " << millis << "\n";*/
		}
	} // else move diagonally
	else
	{
		int x_speed = (abs(x_offset) / max_size_vector);
		x_speed = x_speed + extra_speed_x;
		x_speed = x_speed < max_speed ? x_speed : max_speed;
		int y_speed = (abs(y_offset) / max_size_vector);
		y_speed = y_speed + extra_speed_y;
		y_speed = y_speed < max_speed ? y_speed : max_speed;

		if (x_offset > 0 && y_offset > 0)
		{
			// move right down
			x_speed_ = x_speed;
			y_speed_ = -1 * y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanDownRight(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/
		}
		else if (x_offset > 0 && y_offset <= 0)
		{
			// move right up
			x_speed_ = x_speed;
			y_speed_ = y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanUpRight(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/
		}
		else if (x_offset < 0 && y_offset > 0)
		{
			// move left down
			x_speed_ = -1 * x_speed;
			y_speed_ = -1 * y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanDownLeft(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/
		}
		else
		{
			// move left up
			x_speed_ = -1 * x_speed;
			y_speed_ = y_speed;
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "pan tilt: " << millis << "\n";*/
			tiltPanUpLeft(msg, x_speed, y_speed, camera_address);
			/*now = std::chrono::steady_clock::now();
			duration = now.time_since_epoch();
			millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::cout << "done pan tilt: " << millis << "\n";*/
		}
	}
}

// void callback(
// 	const boost::system::error_code& error,
// 	std::size_t bytes_transferred,
// 	std::shared_ptr<boost::asio::ip::tcp::socket> socket,
// 	std::string str)
// {
// 	if (error)
// 	{
// 		std::cout << error.message() << '\n';
// 	}
// 	else if (bytes_transferred == str.length())
// 	{
// 		std::cout << "Message is sent successfully!" << '\n';
// 	}
// 	else
// 	{
// 		socket->async_send(
// 			boost::asio::buffer(str.c_str() + bytes_transferred, str.length() - bytes_transferred),
// 			std::bind(callback, std::placeholders::_1, std::placeholders::_2, socket, str));
// 	}
// }

// std::string Camera_serial::write_serial(const std::vector<char>& msg, size_t at_least)
// {
// 	std::lock_guard<std::mutex> lock(m_mutex);
// 	// request/message from client
// 	if (m_verbose_debug)
// 	{
// 		cout << "Msg to be sent: " << string(msg.begin(), msg.end()) << endl;
// 	}

// 	boost::system::error_code error;

// 	boost::asio::write(m_active_socket, boost::asio::buffer(msg), error);
// 	if (!error)
// 	{
// 		if (m_verbose_debug)
// 		{
// 			cout << "Client sent message!" << endl;
// 		}
// 	}
// 	else
// 	{
// 		if (m_verbose_debug)
// 		{
// 			cout << "send failed: " << error.message() << endl;
// 		}
// 		m_connected_status = false;
// 		return "";
// 	}
// 	if (m_verbose_debug)
// 	{
// 		cout << "End of sending" << endl;
// 	}

// 	// boost::asio::steady_timer timeout_timer(m_io_service, std::chrono::milliseconds(200));
// 	// timeout_timer.async_wait([&]() { m_active_socket.cancel(); });

// 	// getting response from server
// 	boost::system::error_code read_error;

// 	// boost::asio::read(m_active_socket, receive_buffer, boost::asio::transfer_at_least(at_least), read_error);

// 	boost::optional<boost::system::error_code> timer_result;
// 	boost::asio::deadline_timer timer(m_io_service);
// 	timer.expires_from_now(boost::posix_time::seconds(3));
// 	timer.async_wait([&timer_result](const boost::system::error_code& error)
// 		{ timer_result.reset(error); });

// 	boost::asio::streambuf receive_buffer;
// 	boost::optional<boost::system::error_code> read_result;
// 	boost::asio::async_read(m_active_socket, receive_buffer, boost::asio::transfer_at_least(at_least), [&read_result](const boost::system::error_code& error, size_t)
// 		{ read_result.reset(error); });

// 	m_io_service.reset();
// 	while (m_io_service.run_one())
// 	{
// 		if (read_result)
// 		{
// 			timer.cancel();
// 		}
// 		else if (timer_result)
// 		{
// 			cout << "Receive timeout" << endl;
// 			m_connected_status = false;
// 			m_active_socket.cancel();
// 			return "";
// 		}
// 	}

// 	if (m_verbose_debug)
// 	{
// 		cout << "Receiving done" << endl;
// 	}
// 	if (*read_result && *read_result != boost::asio::error::eof)
// 	{
// 		if (m_verbose_debug)
// 		{
// 			cout << "Receive failed: " << read_result->message() << endl;
// 		}
// 		m_connected_status = false;
// 		return "";
// 	}
// 	else
// 	{
// 		const char* data_c = buffer_cast<const char*>(receive_buffer.data());
// 		/*if (receive_buffer.size()==4 || receive_buffer.size() !=7)
// 		{
// 			std::cout << "[ LOG ] Data received: ";
// 			for (int i = 0; i < receive_buffer.size(); ++i)
// 			{
// 				std::cout << "\\x" << std::setfill('0') << std::hex << std::setw(2) << static_cast<unsigned int>(*(data_c + i));
// 			}
// 			std::cout << std::endl;
// 		}*/
// 		// for (int i = 0; i < receive_buffer.size(); ++i)
// 		////for (int i = receive_buffer.size() == 11 ? 4 : 0; i < receive_buffer.size(); ++i)
// 		//{
// 		//	printf(" %x", *(data_c + i));
// 		//}
// 		// string data((receive_buffer.size() <= 7) ? data_c : (data_c + receive_buffer.size() - 7), receive_buffer.size());
// 		string data(data_c, receive_buffer.size());
// 		/*if (receive_buffer.size()==4 || receive_buffer.size() !=7)
// 			cout << "Received:" << data << ", len: " << std::dec << data.length() << endl;*/
// 		m_connected_status = true;
// 		// cout << "Returning" << endl;
// 		return data;
// 	}
// };

// std::string Camera_serial::write_serial_nores(const std::vector<char>& msg)
// {
// 	std::lock_guard<std::mutex> lock(m_mutex);
// 	// request/message from client
// 	if (m_verbose_debug)
// 	{
// 		cout << "Msg to be sent: " << string(msg.begin(), msg.end()) << endl;
// 	}

// 	boost::system::error_code error;

// 	boost::asio::write(m_active_socket, boost::asio::buffer(msg), error);
// 	if (!error)
// 	{
// 		if (m_verbose_debug)
// 		{
// 			cout << "Client sent message!" << endl;
// 		}
// 	}
// 	else
// 	{
// 		if (m_verbose_debug)
// 		{
// 			cout << "send failed: " << error.message() << endl;
// 		}
// 		m_connected_status = false;
// 		return "";
// 	}
// 	if (m_verbose_debug)
// 	{
// 		cout << "End of sending" << endl;
// 	}
// 	return "OK";
// };

// std::string Camera_serial::write_serial(const std::string& msg, size_t at_least)
// {
// 	write_serial(msg, 1, m_ip_address, m_target_port, at_least);
// };

// std::string Camera_serial::write_serial_nores(const std::string& msg)
// {
// 	write_serial_nores(msg, 1, m_ip_address, m_target_port);
// };

std::vector<std::vector<float>> Camera_serial::loadLut(std::string filename)
{
	// Initialise variables
	std::vector<float> zoom;
	std::vector<float> fov;
	std::vector<std::vector<float>> lut;

	// Two LUT presets below not requiring file
	if (filename == "real" || filename == "Axis")
	{
		zoom.push_back(5000);
		fov.push_back(2.00);

		zoom.push_back(10000);
		fov.push_back(60.00);
	}
	else if (filename == "sim" || filename == "Sony")
	{
		// static cam

		zoom.push_back(0);
		fov.push_back(60.00);

		zoom.push_back(3231);
		fov.push_back(51.68);

		zoom.push_back(4391);
		fov.push_back(49.79);

		zoom.push_back(5666);
		fov.push_back(47.87);

		zoom.push_back(7579);
		fov.push_back(45.93);

		zoom.push_back(9492);
		fov.push_back(43.29);

		zoom.push_back(12362);
		fov.push_back(39.92);

		zoom.push_back(17145);
		fov.push_back(34.38);

		zoom.push_back(20971);
		fov.push_back(30.12);

		zoom.push_back(23521);
		fov.push_back(27.95);

		zoom.push_back(26710);
		fov.push_back(24.44);

		zoom.push_back(30809);
		fov.push_back(20.81);

		zoom.push_back(36275);
		fov.push_back(16.23);

		zoom.push_back(43928);
		fov.push_back(10.76);

		zoom.push_back(49029);
		fov.push_back(7.31);

		zoom.push_back(55406);
		fov.push_back(4.24);

		zoom.push_back(58427);
		fov.push_back(3.47);

		zoom.push_back(61783);
		fov.push_back(2.70);

		zoom.push_back(65535);
		fov.push_back(1.70);
	} // else loading from file
	else
	{
		std::ifstream file(filename);

		if (!file.is_open())
		{
			cout << "ERROR reading LUT" << endl;
		}
		else
		{
			for (std::string line; std::getline(file, line);)
			{
				line = line.substr(0, line.find("#"));
				if (!line.empty())
				{

					// split loaded lines to zoom and fov
					size_t space_delimiter_pos = line.find("\t");

					// load zoom
					std::string zoom_val = line.substr(0, space_delimiter_pos);
					zoom_val = zoom_val.erase(0, zoom_val.find_first_not_of("\t "));
					zoom_val = zoom_val.erase(zoom_val.find_last_not_of("\t ") + 1);

					// loads fov
					std::string fov_val = line.substr(space_delimiter_pos + 1);
					fov_val = fov_val.erase(0, fov_val.find_first_not_of("\t "));
					fov_val = fov_val.erase(fov_val.find_last_not_of("\t ") + 1);

					// loading to respective vectors
					if (m_verbose_debug)
					{
						cout << "LUT loading zoom: [" << zoom_val << "] and fov: [" << fov_val << "]" << endl;
					}
					zoom.push_back(stoi(zoom_val));
					fov.push_back(stof(fov_val));
				}
			}
		}
	}
	lut.push_back(zoom);
	lut.push_back(fov);

	// Set LUT to be contained in object memory
	m_lut = lut;

	for (auto& fov_ : fov)
	{
		if (max_fov == 0)
			max_fov = fov_;
		if (min_fov == 0)
			min_fov = fov_;
		if (fov_ > max_fov)
			max_fov = fov_;
		if (fov_ < min_fov)
			min_fov = fov_;
	}

	return lut;
}

float Camera_serial::zoomToFov(int zoom, std::vector<std::vector<float>> lut)
{
	lut.push_back(std::vector<float>{1, 2, 3 });
	lut.push_back(std::vector<float>{1, 2, 3 });
	// cout << "Zoom to FOV init, zoom: [" << zoom << "]" << endl;
	std::cout << "cac 1 " << zoom << "\n";;
	for (int i = 0; i < lut[0].size(); i++)
	{
		// cout << "i: " << i << " (" << i + 1 << "/" << lut[0].size() << ")" << endl;
		if (i != 0)
		{
			// cout << "Comparing lut[0][i] >= zoom > lut[0][i - 1] : " << lut[0][i] << " >= " << zoom << " > " << lut[0][i - 1] << endl;
		}
		if (i == 0)
		{
			// cout << "i=0; comparing min zoom with equivalent min lut: " << lut[0][0] << endl;
			// If zoom is below minimal zoom, set to min
			if (zoom <= lut[0][0])
			{
				// cout << "returning min zoom equivalent fov: " << lut[1][0] << endl;
				return lut[1][0];
			}
		}
		else if (lut[0][i] >= zoom && zoom > lut[0][i - 1])
		{
			// Fov is the ratio of zoom level range in, times the gap between respective fov levels(negative),
			// plus the larger fov value
			// cout << "TRUE, returning fov: " << ((zoom - lut[0][i - 1]) / (lut[0][i] - lut[0][i - 1])) * (lut[1][i] - lut[1][i - 1]) + lut[1][i - 1] << endl;
			return ((zoom - lut[0][i - 1]) / (lut[0][i] - lut[0][i - 1])) * (lut[1][i] - lut[1][i - 1]) + lut[1][i - 1];
		}
	}
	// If zoom is beyond maximal zoom, set to max
	// cout << "Zoom beyond max, returning equivalent fov" << endl;
	return lut[1][(lut[0].size()) - 1];
}

// TODO some tests
float Camera_serial::fovToZoom(float fov, std::vector<std::vector<float>> lut)
{
	// Is fov descending or ascending?
	bool is_ascending = lut[1][0] < lut[1][lut[0].size() - 1] ? true : false;
	// cout << "asc: " << is_ascending << endl;
	for (int i = 0; i < lut[0].size(); i++)
	{
		if (is_ascending)
		{
			if (i != 0)
			{
				// cout << "Comparing lut[1][i] >= fov > lut[1][i - 1] : " << lut[1][i] << " >= " << fov << " > " << lut[1][i - 1] << endl;
			}
			if (i == 0)
			{
				if (fov <= lut[1][0])
				{
					return lut[0][0];
				}
			}
			else if (lut[1][i] >= fov && fov > lut[1][i - 1])
			{
				return float(int(((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1]));
			}
		}
		else
		{
			if (i != 0)
			{
				// cout << "Comparing lut[1][i] < fov <= lut[1][i - 1] : " << lut[1][i] << " < " << fov << " <= " << lut[1][i - 1] << endl;
			}
			if (i == 0)
			{
				if (fov >= lut[1][0])
				{
					// cout << "RETURNING: " << lut[0][0] << endl;
					return lut[0][0];
				}
			}
			else if (lut[1][i] < fov && fov <= lut[1][i - 1])
			{
				// cout << "RETURNING: " << float(int(((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1])) << endl;
				return float(int(((fov - lut[1][i - 1]) / (lut[1][i] - lut[1][i - 1])) * (lut[0][i] - lut[0][i - 1]) + lut[0][i - 1]));
			}
		}
	}
	cout << "RETURNING: " << lut[0][(lut[0].size()) - 1];
	return lut[0][(lut[0].size()) - 1];
	// If zoom is beyond maximal zoom, set to max
	// cout << "Zoom beyond max, returning equivalent fov" << endl;
}

Camera_serial::~Camera_serial() {
	// cout << "Client destroyed";
};