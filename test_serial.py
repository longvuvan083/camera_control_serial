
import serial
import time

ser = serial.Serial("/dev/ttyUSB1", 9600, bytesize=serial.EIGHTBITS, timeout=15, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE)

def query_position(ser):
    ser.write(b'\xff\x01\x00\x51\x00\x00\x52')
    data = ser.read(7)
    print("PAN: ", data)
    data = data[4:6]
    pan = float(data[0] << 8 | data[1]) / 100
    # print(f"Pan: {pan} deg")
    time.sleep(0.025)
    ser.write(b'\xff\x01\x00\x55\x00\x00\x56')
    data = ser.read(7)
    print("ZOOM: ", data)
    data = data[4:6]
    zoom = data[0] << 8 | data[1]
    time.sleep(0.025)
    ser.write(b'\xff\x01\x00\x53\x00\x00\x54')
    data = ser.read(7)
    print("TILT: ", data)
    data = data[4:6]
    tilt = float(data[0] << 8 | data[1]) / 100
    print(f"Pan: {pan} deg | Tilt: {tilt} deg | Zoom: {zoom}")


# move camera left and stop
# ser.write(b'\xff\x01\x0a\x6b\x00\x00\x76')
# ser.write(b'\xff\x01\x00\x04\x03\x06\x0e')  # Camera 1 moving left with speed 3
# for _ in range(30):
#     query_position(ser)
#     time.sleep(0.3)

# ser.write(b'\xff\x01\x0a\x6b\x00\x00\x76')
# ser.write(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
# query_position(ser)

# # move camera right and stop
# print("Camera 1 moving right")
# ser.write(b'\xff\x01\x00\x02\x03\x06\x0c')  # Camera 1 moving right with speed 3
# for _ in range(33):
#     query_position(ser)
#     time.sleep(1)

# ser.write(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
# query_position(ser)

# # move camera up and stop
# print("Camera 1 moving up")
# ser.write(b'\xff\x01\x00\x08\x03\x06\x12')  # Camera 1 moving up with speed 3
# for _ in range(33):
#     query_position(ser)
#     time.sleep(0.3)

# ser.write(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop
# query_position(ser)

# # move camera down and stop
# print("Camera 1 moving down")
ser.write(b'\xff\x01\x00\x10\x03\x06\x1a')  # Camera 1 moving down with speed 3
for _ in range(33):
    query_position(ser)
    time.sleep(0.3)

# ser.write(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop OK
# query_position(ser)

# # zoom in
# print("Camera 1 zooming in")
# ser.write(b'\xff\x01\x00\x20\x00\x00\x21')  # Camera 1 moving down with speed 3
# for _ in range(33):
#     query_position(ser)
#     time.sleep(0.3)

# ser.write(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop OK
# query_position(ser)

# # zoom out
# print("Camera 1 zooming in")
# ser.write(b'\xff\x01\x00\x40\x00\x00\x41')  # Camera 1 moving down with speed 3
# for _ in range(33):
#     query_position(ser)
#     time.sleep(0.3)

ser.write(b'\xff\x01\x00\x00\x00\x00\x01')  # Camera 1 stop OK
query_position(ser)
